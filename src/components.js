import NavigationDrawerList from './components/NavigationDrawerList'
import NavigationDrawer from './components/NavigationDrawer'

const Components = {
  NavigationDrawer,
  NavigationDrawerList
}

export default Components
